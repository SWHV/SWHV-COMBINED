download = 'jp2download.py'
fits2fits = ''
fits2img = 'fits2img'
virtualenv = { 'VIRTUALENV_PYTHON' : '/swhv/python/bin/python'}
tempdir = "/data/temp/"
datadir = "/data/"

process_tree_config = {
    'hmi_continuum':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [['-b','--begin_date'], ['-e','--end_date']],
        'command' : download + ' -t hmi_continuum -b %%begin_date%% -e %%end_date%% -m %%stations%% -c 3600 -o %%output_file_dir%% -f %%final_dir%%',
        'output_dir_date_format_add' : "/%Y/%m/%d",
        'filename_date_pattern'      : r'([0-9]{4})_([0-9]{2})_([0-9]{2})__([0-9]{2})_([0-9]{2})_([0-9]{2})_([0-9]{1,4})__SDO_HMI_HMI_magnetogram.jp2',
        'abbrev' : 'gong_magnetogram',
        'first' : True,
        'parent' : None,
        'children' : [],
        'reprocess': False,
        'temp_output_dir': tempdir + "downloads/HMI_magnetogram/",
        'output_dir': datadir + "downloads/HMI_magnetogram/",
    },
    'hmi_magnetogram':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [['-b','--begin_date'], ['-e','--end_date']],
        'command' : download + ' -t hmi_magnetogram -b %%begin_date%% -e %%end_date%% -m %%stations%% -c 3600 -o %%output_file_dir%% -f %%final_dir%%',
        'output_dir_date_format_add' : "/%Y/%m/%d",
        'filename_date_pattern'      : r'([0-9]{4})_([0-9]{2})_([0-9]{2})__([0-9]{2})_([0-9]{2})_([0-9]{2})_([0-9]{1,4})__SDO_HMI_HMI_magnetogram.jp2',
        'abbrev' : 'gong_magnetogram',
        'first' : True,
        'parent' : None,
        'children' : [],
        'reprocess': False,
        'temp_output_dir': tempdir + "downloads/HMI_magnetogram/",
        'output_dir': datadir + "downloads/HMI_magnetogram/",
    },
    #Gong magnetogram
    'gong_magnetogram':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [['-b','--begin_date'], ['-e','--end_date'], ['-m', '--stations']],
        'command' : download + ' -t gong_magnetogram -b %%begin_date%% -e %%end_date%% -m %%stations%% -c 3600 -o %%output_file_dir%% -f %%final_dir%%',
        'output_dir_date_format_add' : "/%Y/%m/%d",
        'filename_date_pattern'      : r'[a-z]{5}([0-9]{2})([0-9]{2})([0-9]{2})[a-z]([0-9]{2})([0-9]{2}).fits.gz',
        'abbrev' : 'gong_magnetogram',
        'first' : True,
        'parent' : None,
        'children' : ['fits2fits_gong_magnetogram'],
        'reprocess': False,
        'temp_output_dir': tempdir + "downloads/gong_magnetogram/",
        'output_dir': datadir + "downloads/gong_magnetogram/",
    },
    'fits2fits_gong_magnetogram':
    {
        'environment_vars' : virtualenv,
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2fits + 'fits2fits.py --type gong_magnetogram --input_file %%input_file%% --output_file %%output_file_path%%',
        'abbrev' : 'fits2fits_gong_magnetogram',
        'first' : False,
        'parent' : 'gong_magnetogram',
        'children' : ['fits2img_gong_magnetogram'],
        'temp_output_dir': tempdir + "fits2fits/gong_magnetogram/",
        'output_dir': datadir + "fits2fits/gong_magnetogram/",
        'reprocess': True,
    },
    'fits2img_gong_magnetogram':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2img + ' --min-clip=-700 --max-clip=700 --gamma=1.0 -J %%input_file%% -o %%output_file_dir%%',
        'abbrev' : 'fits2img_gong_magnetogram',
        'first' : False,
        'parent' : 'fits2fits_gong_magnetogram',
        'children' : [],
        'temp_output_dir': tempdir + "localjp2/gong_magnetogram/gong_magnetogram/",
        'output_dir': datadir + "localjp2/gong_magnetogram/gong_magnetogram/",
        'reprocess': True,
    },
    'gong_halpha':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [['-b','--begin_date'], ['-e','--end_date']],
        'command' : download + ' -t gong_halpha -b %%begin_date%% -e %%end_date%% -c 600 -o %%output_file_dir%% -f %%final_dir%%',
        'output_dir_date_format_add' : "/%Y/%m/%d",
        'abbrev' : 'gong_halpha',
        'first' : True,
        'parent' : None,
        'children' : ['fits2fits_gong_halpha'],
        'reprocess': False,
        'temp_output_dir': tempdir + "downloads/gong_halpha/",
        'output_dir': datadir + "downloads/gong_halpha/",
        'filename_date_pattern' : r'([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})[A-Z][a-z].fits.fz',
        'add_to_date' : 0,
    },
    'fits2fits_gong_halpha':
    {
        'environment_vars' : virtualenv,
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2fits + 'fits2fits.py --type gong_halpha --input_file %%input_file%% --output_file %%output_file_path%%',
        'abbrev' : 'fits2fits_gong_halpha',
        'first' : False,
        'parent' : 'gong_halpha',
        'children' : ['fits2img_gong_halpha'],
        'temp_output_dir': tempdir + "fits2fits/gong_halpha/",
        'output_dir': datadir + "fits2fits/gong_halpha/",
        'reprocess': False,
    },
    'fits2img_gong_halpha':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2img + ' --min-clip=-6 --max-clip=4900 --gamma=1.0 -J %%input_file%% -o %%output_file_dir%%',
        'abbrev' : 'fits2img_gong_halpha',
        'first' : False,
        'parent' : 'fits2fits_gong_halpha',
        'children' : [],
        'temp_output_dir': tempdir + "localjp2/gong_halpha/gong_halpha/",
        'output_dir': datadir + "localjp2/gong_halpha/gong_halpha/",
        'reprocess': False,
    },
    #Gong farside
    'gong_farside':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [['-b','--begin_date'], ['-e','--end_date']],
        'command' : download + ' -t gong_farside -b %%begin_date%% -e %%end_date%% -m %%stations%% -c 1 -o %%output_file_dir%% -f %%final_dir%%',
        'output_dir_date_format_add' : "/%Y/%m/%d",
        'filename_date_pattern'      : r'mrfqo([0-9]{2})([0-9]{2})([0-9]{2})t([0-9]{2})([0-9]{2}).fits',
        'abbrev' : 'gong_farside',
        'first' : True,
        'parent' : None,
        'children' : ['fits2fits_gong_farside'],
        'reprocess': False,
        'temp_output_dir': tempdir + "downloads/gong_farside/",
        'output_dir': datadir + "downloads/gong_farside/",
    },
    'fits2fits_gong_farside':
    {
        'environment_vars' : virtualenv,
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2fits + 'fits2fits.py --type gong_farside --input_file %%input_file%% --output_file %%output_file_path%%',
        'abbrev' : 'fits2fits_gong_farside',
        'first' : False,
        'parent' : 'gong_farside',
        'children' : ['fits2img_gong_farside'],
        'temp_output_dir': tempdir + "fits2fits/gong_farside/",
        'output_dir': datadir + "fits2fits/gong_farside/",
        'reprocess': True,
    },
    'fits2img_gong_farside':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2img + ' --min-clip=-0.25 --max-clip=0.25 --gamma=1.0 -J %%input_file%% -o %%output_file_dir%%',
        'abbrev' : 'fits2img_gong_farside',
        'first' : False,
        'parent' : 'fits2fits_gong_farside',
        'children' : [],
        'temp_output_dir': tempdir + "localjp2/gong_farside/gong_farside/",
        'output_dir': datadir + "localjp2/gong_farside/gong_farside/",
        'reprocess': True,
    },
    'solis_v22':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [['-b','--begin_date'], ['-e','--end_date']],
        'command' : download + ' -t solis_v22 -b %%begin_date%% -e %%end_date%% -c 36000 -o %%output_file_dir%% -f %%final_dir%%',
        'output_dir_date_format_add' : "/%Y/%m/%d",
        'abbrev' : 'solis_v22',
        'first' : True,
        'parent' : None,
        'children' : ['fits2fits_solis_v22'],
        'reprocess': False,
        'temp_output_dir': tempdir + "downloads/solis_v22/",
        'output_dir': datadir + "downloads/solis_v22/",
        'filename_date_pattern'      : r'k4v22([0-9]{2})([0-9]{2})([0-9]{2})t([0-9]{2})([0-9]{2})([0-9]{2}).fts.gz',
        'add_to_date' : 2000,
    },
    'fits2fits_solis_v22':
    {
        'environment_vars' : virtualenv,
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2fits + 'fits2fits.py --type solis_v22 --input_file %%input_file%% --output_file %%output_file_path%%',
        'abbrev' : 'fits2fits_solis_v22',
        'first' : False,
        'parent' : 'solis_v22',
        'children' : ['fits2img_solis_v22'],
        'temp_output_dir': tempdir + "fits2fits/solis_v22/",
        'output_dir': datadir + "fits2fits/solis_v22/",
        'reprocess': True,
    },
    'fits2img_solis_v22':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2img + ' --min-clip=-128 --max-clip=150 --gamma=1.0 -J %%input_file%% -o %%output_file_dir%%',
        'abbrev' : 'fits2img_solis_v22',
        'first' : False,
        'parent' : 'fits2fits_solis_v22',
        'children' : [],
        'temp_output_dir': tempdir + "localjp2/solis_v22/solis_v22/",
        'output_dir': datadir + "localjp2/solis_v22/solis_v22/",
        'reprocess': True,
    },
    'solis_v95':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [['-b','--begin_date'], ['-e','--end_date']],
        'command' : download + ' -t solis_v95 -b %%begin_date%% -e %%end_date%% -c 36000 -o %%output_file_dir%% -f %%final_dir%%',
        'output_dir_date_format_add' : "/%Y/%m/%d",
        'abbrev' : 'solis_v95',
        'first' : True,
        'parent' : None,
        'children' : ['fits2fits_solis_v95'],
        'reprocess': False,
        'temp_output_dir': tempdir + "downloads/solis_v95/",
        'output_dir': datadir + "downloads/solis_v95/",
        'filename_date_pattern'      : r'k4v95([0-9]{2})([0-9]{2})([0-9]{2})t([0-9]{2})([0-9]{2})([0-9]{2}).fts.gz',
        'add_to_date' : 2000,
    },
    'fits2fits_solis_v95':
    {
        'environment_vars' : virtualenv,
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2fits + 'fits2fits.py --type solis_v95 --input_file %%input_file%% --output_file %%output_file_path%%',
        'abbrev' : 'fits2fits_solis_v95',
        'first' : False,
        'parent' : 'solis_v95',
        'children' : ['fits2img_solis_v95_inclination', 'fits2img_solis_v95_intensity', 'fits2img_solis_v95_azimuth', 'fits2img_solis_v95_fillfactor', 'fits2img_solis_v95_strength'],
        'temp_output_dir': tempdir + "fits2fits/solis_v95/",
        'output_dir': datadir + "fits2fits/solis_v95/",
        'reprocess': True,
    },
    'fits2img_solis_v95_strength':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2img + ' --min-clip=0 --max-clip=135 --gamma=1.0 -J %%input_file%% -o %%output_file_dir%%',
        'abbrev' : 'fits2img_solis_v95',
        'first' : False,
        'filename_filter_pattern' : r'strength.*',
        'parent' : 'fits2fits_solis_v95',
        'children' : [],
        'temp_output_dir': tempdir + "localjp2/solis_v95/solis_v95/",
        'output_dir': datadir + "localjp2/solis_v95/solis_v95/",
        'reprocess': True,
    },
    'fits2img_solis_v95_inclination':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2img + ' --min-clip=0 --max-clip=135 --gamma=1.0 -J %%input_file%% -o %%output_file_dir%%',
        'abbrev' : 'fits2img_solis_v95',
        'first' : False,
        'filename_filter_pattern' : r'inclination.*',
        'parent' : 'fits2fits_solis_v95',
        'children' : [],
        'temp_output_dir': tempdir + "localjp2/solis_v95/solis_v95/",
        'output_dir': datadir + "localjp2/solis_v95/solis_v95/",
        'reprocess': True,
    },
    'fits2img_solis_v95_fillfactor':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2img + ' --min-clip=0 --max-clip=0.032 --gamma=1.0 -J %%input_file%% -o %%output_file_dir%%',
        'abbrev' : 'fits2img_solis_v95',
        'first' : False,
        'filename_filter_pattern' : r'fillfactor.*',
        'parent' : 'fits2fits_solis_v95',
        'children' : [],
        'temp_output_dir': tempdir + "localjp2/solis_v95/solis_v95/",
        'output_dir': datadir + "localjp2/solis_v95/solis_v95/",
        'reprocess': True,
    },
    'fits2img_solis_v95_intensity':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2img + ' --min-clip=0 --max-clip=45000 --gamma=1.0 -J %%input_file%% -o %%output_file_dir%%',
        'abbrev' : 'fits2img_solis_v95',
        'first' : False,
        'filename_filter_pattern' : r'intensity.*',
        'parent' : 'fits2fits_solis_v95',
        'children' : [],
        'temp_output_dir': tempdir + "localjp2/solis_v95/solis_v95/",
        'output_dir': datadir + "localjp2/solis_v95/solis_v95/",
        'reprocess': True,
    },
    'fits2img_solis_v95_azimuth':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2img + ' --min-clip=-90 --max-clip=90 --gamma=1.0 -J %%input_file%% -o %%output_file_dir%%',
        'abbrev' : 'fits2img_solis_v95',
        'first' : False,
        'filename_filter_pattern' : r'azimuth.*',
        'parent' : 'fits2fits_solis_v95',
        'children' : [],
        'temp_output_dir': tempdir + "localjp2/solis_v95/solis_v95/",
        'output_dir': datadir + "localjp2/solis_v95/solis_v95/",
        'reprocess': True,
    },
    'solis_v82':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [['-b','--begin_date'], ['-e','--end_date']],
        'command' : download + ' -t solis_v82 -b %%begin_date%% -e %%end_date%% -c 36000 -o %%output_file_dir%% -f %%final_dir%%',
        'output_dir_date_format_add' : "/%Y/%m/%d",
        'abbrev' : 'solis_v82',
        'first' : True,
        'parent' : None,
        'children' : ['fits2fits_solis_v82'],
        'reprocess': False,
        'temp_output_dir': tempdir + "downloads/solis_v82/",
        'output_dir': datadir + "downloads/solis_v82/",
        'filename_date_pattern'      : r'k4v82([0-9]{2})([0-9]{2})([0-9]{2})t([0-9]{2})([0-9]{2})([0-9]{2}).fts.gz',
        'add_to_date' : 2000,
    },
    'fits2fits_solis_v82':
    {
        'environment_vars' : virtualenv,
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2fits + 'fits2fits.py --type solis_v82 --input_file %%input_file%% --output_file %%output_file_path%%',
        'abbrev' : 'fits2fits_solis_v82',
        'first' : False,
        'parent' : 'solis_v82',
        'children' : ['fits2img_solis_v82_corewingintensity', 'fits2img_solis_v82_corefluxdens'],
        'temp_output_dir': tempdir + "fits2fits/solis_v82/",
        'output_dir': datadir + "fits2fits/solis_v82/",
        'reprocess': True,
    },
    'fits2img_solis_v82_corefluxdens':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2img + ' --min-clip=-15 --max-clip=15 --gamma=1.0 -J %%input_file%% -o %%output_file_dir%%',
        'filename_filter_pattern' : r'corefluxdens.*',
        'abbrev' : 'fits2img_solis_v82_corefluxdens',
        'first' : False,
        'parent' : 'fits2fits_solis_v82',
        'children' : [],
        'temp_output_dir': tempdir + "localjp2/solis_v82/solis_v82/",
        'output_dir': datadir + "localjp2/solis_v82/solis_v82/",
        'reprocess': True,
    },
    'fits2img_solis_v82_corewingintensity':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2img + ' --min-clip=-24 --max-clip=24 --gamma=1.0 -J %%input_file%% -o %%output_file_dir%%',
        'filename_filter_pattern' : r'corewingintensity.*',
        'abbrev' : 'fits2img_solis_v82',
        'first' : False,
        'parent' : 'fits2fits_solis_v82',
        'children' : [],
        'temp_output_dir': tempdir + "localjp2/solis_v82/solis_v82/",
        'output_dir': datadir + "localjp2/solis_v82/solis_v82/",
        'reprocess': True,
    },
    #callisto
    'callisto':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [['-b','--begin_date'], ['-e','--end_date'],['-s', '--station']],
        'filename_date_pattern'      : r'.*_([0-9]{4})([0-9]{2})([0-9]{2})_([0-9]{2})([0-9]{2})([0-9]{2})_[0-9]{2}.fit.gz', 
        'command' : download + ' -t callisto -b %%begin_date%% -e %%end_date%% --stations=%%station%% -c 0 -o %%output_file_dir%% -f %%final_dir%%',
        'output_dir_date_format_add' : "/%Y/%m/%d",
        'abbrev' : 'callisto',
        'first' : True,
        'parent' : None,
        'children' : ["fits2fits_callisto"],
        'reprocess': True,
        'temp_output_dir': tempdir + "downloads/callisto/",
        'output_dir': datadir + "downloads/callisto/",
    },
    'fits2fits_callisto':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [['-b','--begin_date'], ['-e','--end_date']],
        'command_params':[],
        'command' : fits2fits + 'fits2fitscallisto.py --output_dir %%output_file_dir%% --input_directory '+datadir+'downloads/callisto/  --starttime %%begin_date%% --endtime %%end_date%% --startfrequency 20 --endfrequency 400 --deltatime 1 --deltafrequency 1',
        'abbrev' : 'fits2fits_callisto',
        'first' : False,
        'parent' : 'callisto',
        'children' : ["fits2img_callisto"],
        'temp_output_dir': tempdir + "fits2fits/callisto/",
        'output_dir': datadir + "fits2fits/callisto/",
        'reprocess': True,
        'run_once' : True,
    },
    'fits2img_callisto':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2img + ' --min-clip=0 --max-clip=255 --gamma=1.0 -J %%input_file%% -o %%output_file_dir%%',
        'abbrev' : 'fits2img_callisto',
        'first' : False,
        'parent' : 'fits2fits_callisto',
        'children' : [],
        'temp_output_dir': tempdir + "localjp2/callisto/callisto/",
        'output_dir': datadir + "localjp2/callisto/callisto/",
        'reprocess': True,
    },

    #callisto_10s
    'callisto_10s':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [['-b','--begin_date'], ['-e','--end_date'],['-s', '--station']],
        'filename_date_pattern'      : r'.*_([0-9]{4})([0-9]{2})([0-9]{2})_([0-9]{2})([0-9]{2})([0-9]{2})_[0-9]{2}.fit.gz', 
        'command' : download + ' -t callisto -b %%begin_date%% -e %%end_date%% --stations=%%station%% -c 0 -o %%output_file_dir%% -f %%final_dir%%',
        'output_dir_date_format_add' : "/%Y/%m/%d",
        'abbrev' : 'callisto_10s',
        'first' : True,
        'parent' : None,
        'children' : ["fits2fits_callisto_10s"],
        'reprocess': True,
        'temp_output_dir': tempdir + "downloads/callisto_10s/",
        'output_dir': datadir + "downloads/callisto_10s/",
    },
    'fits2fits_callisto_10s':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [['-b','--begin_date'], ['-e','--end_date']],
        'command_params':[],
        'command' : fits2fits + 'fits2fitscallisto.py --output_dir %%output_file_dir%% --input_directory '+datadir+'downloads/callisto/  --starttime %%begin_date%% --endtime %%end_date%% --startfrequency 20 --endfrequency 400 --deltatime 10 --deltafrequency 1',
        'abbrev' : 'fits2fits_callisto_10s',
        'first' : False,
        'parent' : 'callisto_10s',
        'children' : ["fits2img_callisto_10s"],
        'temp_output_dir': tempdir + "fits2fits/callisto_10s/",
        'output_dir': datadir + "fits2fits/callisto_10s/",
        'reprocess': True,
        'run_once' : True,
    },
    'fits2img_callisto_10s':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2img + ' --min-clip=0 --max-clip=255 --gamma=1.0 -J %%input_file%% -o %%output_file_dir%%',
        'abbrev' : 'fits2img_callisto',
        'first' : False,
        'parent' : 'fits2fits_callisto_10s',
        'children' : [],
        'temp_output_dir': tempdir + "localjp2/callisto_10s/callisto_10s/",
        'output_dir': datadir + "localjp2/callisto_10s/callisto_10s/",
        'reprocess': True,
    },
    'rob_uset':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [['-b','--begin_date'], ['-e','--end_date']],
        'filename_date_pattern'      : r'UCH([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2}).FTS',
        'command' : download + ' -t rob_uset -b %%begin_date%% -e %%end_date%% -c 1 -o %%output_file_dir%% -f %%final_dir%%',
        'output_dir_date_format_add' : "/%Y/%m/%d",
        'abbrev' : 'rob_uset',
        'first' : True,
        'parent' : None,
        'children' : ["fits2fits_rob_uset"],
        'reprocess': False,
        'temp_output_dir': tempdir + "downloads/rob_uset/",
        'output_dir': datadir + "downloads/rob_uset/",
    },
    'fits2fits_rob_uset':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [],
        'command_params':[],
        'command' : fits2fits + 'fits2fits.py --type rob_uset --input_file %%input_file%% --output_file %%output_file_path%%',
        'abbrev' : 'fits2fits_rob_uset',
        'first' : False,
        'parent' : 'rob_uset',
        'children' : ["fits2img_rob_uset"],
        'temp_output_dir': tempdir + "fits2fits/rob_uset/",
        'output_dir': datadir + "fits2fits/rob_uset/",
        'reprocess': True,
    },
    'fits2img_rob_uset':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2img + ' --min-clip=0 --max-clip=3500 --gamma=1.0 -J %%input_file%% -o %%output_file_dir%%',
        'abbrev' : 'fits2img_rob_uset',
        'first' : False,
        'parent' : 'fits2fits_rob_uset',
        'children' : [],
        'temp_output_dir': tempdir + "localjp2/rob_uset/rob_uset/",
        'output_dir': datadir + "localjp2/rob_uset/rob_uset/",
        'reprocess': True,
    },

    #kanzelhoehe
    'kanzelhoehe':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [['-b','--begin_date'], ['-e','--end_date']],
        'filename_date_pattern'      : r'kanz_halph_fc_([0-9]{4})([0-9]{2})([0-9]{2})_([0-9]{2})([0-9]{2})([0-9]{2}).fts.gz',
        'command' : download + ' -t kanzelhoehe -b %%begin_date%% -e %%end_date%% -c 1 -o %%output_file_dir%% -f %%final_dir%%',
        'output_dir_date_format_add' : "/%Y/%m/%d",
        'abbrev' : 'kanzelhoehe',
        'first' : True,
        'parent' : None,
        'children' : ["fits2fits_kanzelhoehe"],
        'reprocess': False,
        'temp_output_dir': tempdir + "downloads/kanzelhoehe/",
        'output_dir': datadir + "downloads/kanzelhoehe/",
    },
    'fits2fits_kanzelhoehe':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [],
        'command_params':[],
        'command' : fits2fits + 'fits2fits.py --type kanzelhoehe --input_file %%input_file%% --output_file %%output_file_path%%',
        'abbrev' : 'fits2fits_kanzelhoehe',
        'first' : False,
        'parent' : 'kanzelhoehe',
        'children' : ["fits2img_kanzelhoehe"],
        'temp_output_dir': tempdir + "fits2fits/kanzelhoehe/",
        'output_dir': datadir + "fits2fits/kanzelhoehe/",
        'reprocess': True,
    },
    'fits2img_kanzelhoehe':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2img + ' --min-clip=0 --max-clip=170 --gamma=1.0 -J %%input_file%% -o %%output_file_dir%%',
        'abbrev' : 'fits2img_kanzelhoehe',
        'first' : False,
        'parent' : 'fits2fits_kanzelhoehe',
        'children' : [],
        'temp_output_dir': tempdir + "localjp2/kanzelhoehe/kanzelhoehe/",
        'output_dir': datadir + "localjp2/kanzelhoehe/kanzelhoehe/",
        'reprocess': True,
    },
    #Nancay
    'nrh':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [['-b','--begin_date'], ['-e','--end_date']],
        'filename_date_pattern'      : r'nrh2_1509_h80_([0-9]){4}([0-9]{2})([0-9]{2})_([0-9]{2})([0-9]{2})([0-9]{2})c05_q.fts',
        'command' : download + ' -t nrh -b %%begin_date%% -e %%end_date%% -c 1 -o %%output_file_dir%% -f %%final_dir%%',
        'output_dir_date_format_add' : "/%Y/%m/%d",
        'abbrev' : 'nrh',
        'first' : True,
        'parent' : None,
        'children' : ["fits2fits_nrh"],
        'reprocess': True,
        'temp_output_dir': tempdir + "downloads/nrh/",
        'output_dir': datadir + "downloads/nrh/",
    },
    'fits2fits_nrh':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [],
        'command_params':[],
        'command' : fits2fits + 'fits2fitsnrh.py --input_file %%input_file%% --output_dir %%output_file_dir%%',
        'abbrev' : 'fits2fits_nrh',
        'first' : False,
        'parent' : 'nrh',
        'children' : ["fits2img_nrh"],
        'temp_output_dir': tempdir + "fits2fits/nrh/",
        'output_dir': datadir + "fits2fits/nrh/",
        'reprocess': True,
    },
    'fits2img_nrh':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : fits2img + ' --min-clip=0 --max-clip=1000000 --gamma=1.0 -J %%input_file%% -o %%output_file_dir%%',
        'abbrev' : 'fits2img_nrh',
        'first' : False,
        'parent' : 'fits2fits_nrh',
        'children' : [],
        'temp_output_dir': tempdir + "localjp2/nrh/nrh/",
        'output_dir': datadir + "localjp2/nrh/nrh/",
        'reprocess': True,
    },
    'gongpfss':
    {
        'environment_vars' : virtualenv,
        'command_line_parameters' : [['-b','--begin_date'], ['-e','--end_date']],
        'filename_date_pattern'      : r'mrbqs([0-9]{2})([0-9]{2})([0-9]{2})t([0-9]{2})([0-9]{2})c[0-9]{4}_[0-9]{3}.fits.gz',
        'command' : download + ' -t gong_pfss -b %%begin_date%% -e %%end_date%% -c 21600 -o %%output_file_dir%% -f %%final_dir%%',
        'output_dir_date_format_add' : "/%Y/%m/%d",
        'abbrev' : 'gongpfss',
        'first' : True,
        'parent' : None,
        'children' : ["pfssgunzip"],
        'reprocess': False,
        'temp_output_dir': tempdir + "downloads/pfss/",
        'output_dir': datadir + "downloads/pfss/",
    },
    'pfssgunzip':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : 'gunzip -c %%input_file%% > %%output_file_dir%%`basename %%input_file%%| rev| cut -d"." -f2- |rev `',
        'abbrev' : 'pfssgunzip',
        'first' : False,
        'parent' : 'gongpfss',
        'children' : ['pfss'],
        'temp_output_dir': tempdir + "pfssgunzip/",
        'output_dir': datadir + "pfssgunzip/",
        'reprocess': True,
    },
    'pfss':
    {
        'environment_vars' : {},
        'command_line_params' : [],
        'command_params':[],
        'command' : 'outfilename="%%output_file_dir%%`basename %%input_file%%| rev| cut -d"." -f2- |rev `" && /swhv/bin/pfss %%input_file%%  $outfilename',
        'abbrev' : 'pfss',
        'first' : False,
        'parent' : 'pfssgunzip',
        'children' : [],
        'temp_output_dir': tempdir + "pfss/",
        'output_dir': datadir + "pfss/",
        'reprocess': True,
    },
}


