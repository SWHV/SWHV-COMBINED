<?php
    function get_lookup_array_datasets(){
	    $lookuparray = array ();
	    $subarr = array('baseUrl'=>'http://lasp.colorado.edu/eve/data_access/service/retrieve_data/cgi-bin/retrieve_l2_averages.cgi?',
			 'name'=>'AIA_A94', 
			 'internal_name'=>'AIA_A94', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'AIA_A131', 
			 'internal_name'=>'AIA_A131', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'AIA_A171', 
			 'internal_name'=>'AIA_A171', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'AIA_A193', 
			 'internal_name'=>'AIA_A193', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'AIA_A211', 
			 'internal_name'=>'AIA_A211', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'AIA_A304', 
			 'internal_name'=>'AIA_A304', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'AIA_A335', 
			 'internal_name'=>'AIA_A335', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'E37-45', 
			 'internal_name'=>'E37-45', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'E7-37', 
			 'internal_name'=>'E7-37', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'GOES-14_EUV-A', 
			 'internal_name'=>'GOES-14_EUV-A', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'GOES-14_EUV-B', 
			 'internal_name'=>'GOES-14_EUV-B', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'MA171', 
			 'internal_name'=>'MA171', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'MA257', 
			 'internal_name'=>'MA257', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'MA304', 
			 'internal_name'=>'MA304', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'MA366', 
			 'internal_name'=>'MA366', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'MEGS-A1', 
			 'internal_name'=>'MEGS-A1', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'MEGS-A2', 
			 'internal_name'=>'MEGS-A2', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'MEGS-B_both', 
			 'internal_name'=>'MEGS-B_both', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'MEGS-B_short', 
			 'internal_name'=>'MEGS-B_short', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'ESP171', 
			 'internal_name'=>'ESP171', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'ESP257', 
			 'internal_name'=>'ESP257', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'ESP304', 
			 'internal_name'=>'ESP304', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'ESP366', 
			 'internal_name'=>'ESP366', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'ESPQ', 
			 'internal_name'=>'ESPQ', 
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'MEGSP1216',
			 'internal_name'=>'MEGSP1216',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'Fe_XVIII_94',
			 'internal_name'=>'Fe_XVIII_94',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'Fe_VIII_131',
			 'internal_name'=>'Fe_VIII_131',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'Fe_XX_133',
			 'internal_name'=>'Fe_XX_133',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'Fe_IX_171',
			 'internal_name'=>'Fe_IX_171',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'Fe_X_177',
			 'internal_name'=>'Fe_X_177',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'Fe_XI_180',
			 'internal_name'=>'Fe_XI_180',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'Fe_XII_195',
			 'internal_name'=>'Fe_XII_195',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'Fe_XIII_202',
			 'internal_name'=>'Fe_XIII_202',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'Fe_XIV_211',
			 'internal_name'=>'Fe_XIV_211',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'He_II_256',
			 'internal_name'=>'He_II_256',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'Fe_XV_284',
			 'internal_name'=>'Fe_XV_284',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'He_II_304',
			 'internal_name'=>'He_II_304',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'Fe_XVI_335',
			 'internal_name'=>'Fe_XVI_335',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'Fe_XVI_361',
			 'internal_name'=>'Fe_XVI_361',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'Mg_IX_368',
			 'internal_name'=>'Mg_IX_368',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'Ne_VII_465',
			 'internal_name'=>'Ne_VII_465',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'Si_XII_499',
			 'internal_name'=>'Si_XII_499',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'O_III_526',
			 'internal_name'=>'O_III_526',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'O_IV_554',
			 'internal_name'=>'O_IV_554',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'He_I_584',
			 'internal_name'=>'He_I_584',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'O_III_600',
			 'internal_name'=>'O_III_600',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'O_III_600',
			 'internal_name'=>'O_III_600',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'Mg_X_625',
			 'internal_name'=>'Mg_X_625',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'name'=>'O_V_630',
			 'internal_name'=>'O_V_630',
			 'multiplier'=>1e-6,
			 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'ODI',
		         'dbtable'=>'dataset_goes_gp_xr_1m_rt',
			 'internal_name'=>'fxdo1',
			 'multiplier'=>1e-6,
			 'error_level_min'=>-100000,
			 'error_level_max'=>100000,
			 'name'=>'GOES_XRSA_ODI'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'ODI',
		         'dbtable'=>'dataset_goes_gp_xr_1m_rt',
			 'internal_name'=>'fxdo2',
			 'multiplier'=>1e-6,
			 'error_level_min'=>-100000,
			 'error_level_max'=>100000,
			 'name'=>'GOES_XRSB_ODI'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'ODI',
			 'internal_name'=>'xrsa',
			 'name'=>'EVE_XRSA_ODI',
			 'error_level_min'=>-1,
		         'dbtable'=>'dataset_eve_locs_diode'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'ODI',
			 'internal_name'=>'xrsb',
			 'name'=>'EVE_XRSB_ODI',
			 'error_level_min'=>-1,
		         'dbtable'=>'dataset_eve_locs_diode'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'ODI',
			 'internal_name'=>'channel1',
			 'name'=>'PROBA2_LYRA_LYMAN_ALPHA_ODI',
			 'multiplier'=>1e-3,
			 'error_level_min'=>-1,
		         'dbtable'=>'dataset_proba2_lyra'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'ODI',
			 'name'=>'PROBA2_LYRA_HERZBERG_ODI',
			 'internal_name'=>'channel2',
			 'multiplier'=>1e-3,
			 'error_level_min'=>-1,
		         'dbtable'=>'dataset_proba2_lyra'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'ODI',
			 'internal_name'=>'channel3',
			 'name'=>'PROBA2_LYRA_ALUMINIUM_ODI',
			 'multiplier'=>1e-3,
			 'error_level_min'=>-1,
		         'dbtable'=>'dataset_proba2_lyra'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'ODI',
			 'internal_name'=>'channel4',
			 'name'=>'PROBA2_LYRA_ZIRCONIUM_ODI',
			 'multiplier'=>1e-3,
			 'error_level_min'=>-1,
		         'dbtable'=>'dataset_proba2_lyra'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'ODI',
			 'internal_name'=>'esp_17_1',
			 'name'=>'EVE_esp_17_1_ODI',
			 'error_level_min'=>-1,
		         'dbtable'=>'dataset_eve_locs_diode'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'ODI',
			 'internal_name'=>'esp_25_7',
			 'name'=>'EVE_esp_25_7_ODI',
			 'error_level_min'=>-1,
		         'dbtable'=>'dataset_eve_locs_diode'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'ODI',
			 'internal_name'=>'esp_30_4',
			 'name'=>'EVE_esp_30_4_ODI',
			 'error_level_min'=>-1,
		         'dbtable'=>'dataset_eve_locs_diode'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'ODI',
			 'internal_name'=>'esp_36_6',
			 'name'=>'EVE_esp_36_6_ODI',
			 'error_level_min'=>-1,
		         'dbtable'=>'dataset_eve_locs_diode'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'ODI',
			 'internal_name'=>'megs_p_121_6',
			 'name'=>'EVE_megs_p_121_6_ODI',
			 'error_level_min'=>-1,
		         'dbtable'=>'dataset_eve_locs_diode'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'ODI',
			 'internal_name'=>'espquad',
			 'name'=>'EVE_espquad_ODI',
			 'error_level_min'=>-1,
		         'dbtable'=>'dataset_eve_locs_diode'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'STAFF',
			 'internal_name'=>'short',
			 'name'=>'GOES_XRSA_STAFF',
			 'multiplier'=>1e-6,
			 'error_level_min'=>-100000,
			 'error_level_max'=>100000,
		         'dbtable'=>'goesxraydata'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'STAFF',
			 'internal_name'=>'long',
			 'name'=>'GOES_XRSB_STAFF',
			 'multiplier'=>1e-6,
			 'error_level_min'=>-100000,
			 'error_level_max'=>100000,
		         'dbtable'=>'goesxraydata'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'STAFF',
			 'internal_name'=>'alpha',
			 'name'=>'PROBA2_LYRA_LYMAN_ALPHA_STAFF',
			 'multiplier'=>1e-3,
		         'dbtable'=>'lyraeuvdata_mi'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'STAFF',
			 'internal_name'=>'herzberg',
			 'name'=>'PROBA2_LYRA_HERZBERG_STAFF',
			 'multiplier'=>1e-3,
		         'dbtable'=>'lyraeuvdata_mi'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'STAFF',
			 'internal_name'=>'aluminium',
			 'name'=>'PROBA2_LYRA_ALUMINIUM_STAFF',
			 'multiplier'=>1e-3,
		         'dbtable'=>'lyraeuvdata_mi'
		 );
	    array_push($lookuparray, $subarr);
	    $subarr = array(
			 'type'=>'STAFF',
			 'internal_name'=>'zirkonium',
			 'name'=>'PROBA2_LYRA_ZIRCONIUM_STAFF',
			 'multiplier'=>1e-3,
		         'dbtable'=>'lyraeuvdata_mi'
		 );
	    array_push($lookuparray, $subarr);

	    return $lookuparray;
    }
