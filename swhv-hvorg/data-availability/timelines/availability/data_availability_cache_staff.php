<?php
include("lookup.php");
ini_set('memory_limit','512M');
    date_default_timezone_set('UTC');
    $conn = pg_connect("host=sdobase.oma.be port=5432 dbname=staff_prod user=reader password=Staff.1");
    if (!$conn) {
	die("A database error occurred.\n");
    }
    function readJsonFile($filename){
        if( !file_exists($filename) ){
            return array();
        }
        $filecontents = file_get_contents($filename);
        if($filecontents!=""){
            $json = json_decode($filecontents, true);
            return $json["coverage"];
        }
        else{
            return array();
        }
    }
    function getFilenameAndGapFromDb($conn, $imagesrc){    
        $datasets_array_internal = get_lookup_array_datasets();
	$found = false;
	$max = count($datasets_array_internal);
	$i = 0;
	while(!$found && $i<$max) {
	    if(array_key_exists("dbtable", $datasets_array_internal[$i])){
		if($datasets_array_internal[$i]["dbtable"]==$imagesrc){
		    $found = true;
		}
		else{
		    $i = $i+1;
		}
	    }
	    else{
		$i = $i + 1;
	    }
	}
	if(!$found){
	    pg_close($conn);
	    die("Dataset does not exist: ".$imagesrc."\n");
	}
        return array("dbtable"=>$datasets_array_internal[$i]["dbtable"], "gap"=>60*10);
    }    
    parse_str(implode('&', array_slice($argv, 1)), $_GET);
    $imagesrc= strip_tags($_GET["imagesrc"]);
    $filename_gap = getFilenameAndGapFromDb($conn, $imagesrc);
    if($filename_gap["dbtable"]==""){
            echo json_encode(array( "error"=>"The requested datasource does not exist.")); 
    }
    else{
	//Read existing coverage intervals
        $coverage = readJsonFile(dirname(__FILE__)."/available/".$filename_gap['dbtable'].".json");
        $i = max(0, count($coverage)-1);
        $found = false;
        $now = time();
	$query = "";
	//Remove last intervals for 3 days
        while($i>0 &&!$found){
            if($now - $coverage[$i][0] > 60*60*24*3){
                $found = true;
		$start_date = strftime("%Y-%m-%d %H:%M:%S", $coverage[$i][0]);
		$dbtable = $filename_gap['dbtable'];
		$nowstr = strftime("%Y-%m-%d %H:%M:%S", $now);
		$query="SELECT dts FROM ".$dbtable." where dts BETWEEN '".$start_date."' AND '".$nowstr."' ORDER BY dts;";
                unset($coverage[$i]);
                $i = $i+1;
            }
            else{
                unset($coverage[$i]);
            }
            $i--;
	}
	//$i contains number of intervals
	//Create query if not existing
        if($query==""){
	    $query="SELECT dts FROM ".$filename_gap['dbtable']." ORDER BY dts;";
	}
        $result = pg_query($conn, $query);
        $coverage_subarray = array();
        $firsttime = true;
        while($result && $row = pg_fetch_row($result))
        {
            if($firsttime){
                $prevdt = strtotime($row[0]);
                $firsttime = false;
                $coverage_subarray[0] = $prevdt;
            }
            $dt = strtotime($row[0]);
            if($dt - $prevdt> $filename_gap['gap']){
                    $coverage_subarray[1] = $prevdt;
                    $coverage[$i] =  $coverage_subarray;
                    $i = $i+1;
                    $coverage_subarray[0] = $dt;
            }
            $prevdt = $dt;
        }
        if(!$firsttime){
            $coverage_subarray[1] = $dt;
            $coverage[$i] =  $coverage_subarray;
            $i = $i+1;
        }
        $ja = json_encode(array( "coverage"=>$coverage)); 
        file_put_contents(dirname(__FILE__)."/available/".$filename_gap['dbtable'].".json",$ja);
      }
      pg_close($conn);
