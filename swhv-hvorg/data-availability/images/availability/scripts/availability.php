<!DOCTYPE html>
<meta charset="utf-8">
<style>

body {
  font: 10px sans-serif;
  shape-rendering: crispEdges;
}

.day {
  fill: #fff;
  stroke: #ccc;
}

.month {
  fill: none;
  stroke: #000;
  stroke-width: 2px;
}

.RdYlGn .q0-11{fill:rgb(165,0,38)}
.RdYlGn .q1-11{fill:rgb(215,48,39)}
.RdYlGn .q2-11{fill:rgb(244,109,67)}
.RdYlGn .q3-11{fill:rgb(253,174,97)}
.RdYlGn .q4-11{fill:rgb(254,224,139)}
.RdYlGn .q5-11{fill:rgb(255,255,191)}
.RdYlGn .q6-11{fill:rgb(217,239,139)}
.RdYlGn .q7-11{fill:rgb(166,217,106)}
.RdYlGn .q8-11{fill:rgb(102,189,99)}
.RdYlGn .q9-11{fill:rgb(26,152,80)}
.RdYlGn .q10-11{fill:rgb(0,104,55)}

</style>
<body>
<?php
date_default_timezone_set('UTC');

$con = mysqli_connect("localhost","helioviewer","helioviewer","helioviewer");
if (mysqli_connect_errno())
{
    echo "Failed to connect to database: " . mysqli_connect_error();
}

$query="SELECT id, name, description, enabled FROM helioviewer.datasources";
$result = mysqli_query($con, $query);
$row = mysqli_fetch_row($result);
while($row){
    if($row[3]==1){
	    $image_id = "IID".$row[0];
	    echo '<a href = "#'.$image_id.'">';
	    echo '</a>';
	    echo '<h2>'.$row[2].'</h2>';
	    echo '<div id = "'.$image_id.'"></div>';
    }
    $row = mysqli_fetch_row($result);
}
?>
<script src="http://d3js.org/d3.v3.min.js"></script>
<script>
var createAvail = function(divname, url){
  var width = 960,
	  height = 153,
	  cellSize = 17; // cell size

  var day = d3.time.format("%w"),
	  week = d3.time.format("%U"),
	  percent = d3.format(".1%"),
	  format = d3.time.format("%Y-%m-%d");

  var color = d3.scale.quantize()
	  .domain([0., 1.])
	  .range(d3.range(11).map(function(d) { return "q" + d + "-11"; }));
  var svg = d3.select("body").selectAll("svg");
  var svg = d3.select("#" + divname).selectAll("svg")
	  .data(d3.range(2014, new Date().getFullYear()+1))
	.enter().append("svg")
	  .attr("width", width)
	  .attr("height", height)
	  .attr("class", "RdYlGn")
	.append("g")
	  .attr("transform", "translate(" + ((width - cellSize * 53) / 2) + "," + (height - 17 - cellSize * 7 - 1) + ")");

  svg.append("text")
	  .attr("transform", "translate(-6," + cellSize * 3.5 + ")rotate(-90)")
	  .style("text-anchor", "middle")
	  .text(function(d) { return d; });
  var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  for(var i=0; i< months.length; i++){ 
    svg.append("text")
	  .attr("transform", "translate("+(cellSize*2.5+4.3*i*cellSize)+"," + cellSize * 7.7 + ")")
	  .style("text-anchor", "middle")
	  .text(months[i]);
  }

  var rect = svg.selectAll(".day")
	  .data(function(d) { return d3.time.days(new Date(d, 0, 1), new Date(d + 1, 0, 1)); })
	.enter().append("rect")
	  .attr("class", "day")
	  .attr("width", cellSize)
	  .attr("height", cellSize)
	  .attr("x", function(d) { return week(d) * cellSize; })
	  .attr("y", function(d) { return day(d) * cellSize; })
	  .datum(format);

  rect.append("title")
	  .text(function(d) { return d; });

  svg.selectAll(".month")
	  .data(function(d) { return d3.time.months(new Date(d, 0, 1), new Date(d + 1, 0, 1)); })
	.enter().append("path")
	  .attr("class", "month")
	  .attr("d", monthPath);
	
  function pad(num, size) {
	  var s = num+"";
	  while (s.length < size) s = "0" + s;
	  return s;
  }
  d3.json(url, function(error, csv) {
	var data = {};
	var cov = csv.coverage;
 	var dayjump = 60*60*24;
	for(var i = 0; i<cov.length; i++){
	  var iterations = (cov[i][1] - cov[i][0])/dayjump;
          var cur = cov[i][0];
          var end = cov[i][1];
	  if(end - cur>dayjump){
		  var nd = new Date((cur)*1000);
		  var datestring = "" + nd.getUTCFullYear() + "-" + pad((nd.getUTCMonth()+1), 2) + "-" + pad(nd.getUTCDate(), 2);	  
		  if(data[datestring]!==undefined){
			data[datestring] += 1 - (cur%dayjump)/(dayjump);
		  }
		  else{
			data[datestring] = 1 - (cur%dayjump)/(dayjump);
		  }
		  cur = cur + (dayjump - (cur%dayjump));
		  while(cur +dayjump<=end){
			  var nd = new Date((cur)*1000);
			  var datestring = "" + nd.getUTCFullYear() + "-" + pad((nd.getUTCMonth()+1), 2) + "-" + pad(nd.getUTCDate(), 2);	  
			  data[datestring] = 1.;
			  cur = cur + dayjump;
		  }
		  var nd = new Date((end)*1000);
		  var datestring = "" + nd.getUTCFullYear() + "-" + pad((nd.getUTCMonth()+1), 2) + "-" + pad(nd.getUTCDate(), 2);	  
		  if(data[datestring]!==undefined){
			data[datestring] += (end%dayjump)/(dayjump);
		  }
		  else{
			data[datestring] = (end%dayjump)/(dayjump);
		  }
	  }
	  else{
		  var nd = new Date((cur)*1000);
		  var datestring = "" + nd.getUTCFullYear() + "-" + pad((nd.getUTCMonth()+1), 2) + "-" + pad(nd.getUTCDate(), 2);	  
		  var nde = new Date((end)*1000);
		  var datestringend = "" + nde.getUTCFullYear() + "-" + pad((nde.getUTCMonth()+1), 2) + "-" + pad(nde.getUTCDate(), 2);	  
		  if(datestring===datestringend){
			  if(data[datestring]!==undefined){
				data[datestring] += ((end-cur)%dayjump)/(dayjump);
			  }
			  else{
				data[datestring] = ((end-cur)%dayjump)/(dayjump);
			  }
		  }
		  else{
			  if(data[datestring]!==undefined){
				data[datestring] += 1.-(cur%dayjump)/(dayjump);
			  }
			  else{
				data[datestring] = 1. - (cur%dayjump)/(dayjump);
			  }
			  if(data[datestringend]!==undefined){
				data[datestringend] += 1.-(end%dayjump)/(dayjump);
			  }
			  else{
				data[datestringend] = 1. - (end%dayjump)/(dayjump);
			  }
		  }
	  }
	}


	rect.filter(function(d) { return d in data; })
		.attr("class", function(d) { 
		  return "day " + color(data[d]-0.09); 
		})
	  .select("title")
		.text(function(d) { return d + ": " + percent(data[d]); });
  });

  function monthPath(t0) {
	var t1 = new Date(t0.getFullYear(), t0.getMonth() + 1, 0),
		d0 = +day(t0), w0 = +week(t0),
		d1 = +day(t1), w1 = +week(t1);
	return "M" + (w0 + 1) * cellSize + "," + d0 * cellSize
		+ "H" + w0 * cellSize + "V" + 7 * cellSize
		+ "H" + w1 * cellSize + "V" + (d1 + 1) * cellSize
		+ "H" + (w1 + 1) * cellSize + "V" + 0
		+ "H" + (w0 + 1) * cellSize + "Z";
  }
}
<?php
	$result = mysqli_query($con, $query);
	$row = mysqli_fetch_row($result);
	while($row){
	    if($row[3]==1){
		$image_id = $row[0];
		$url = "http://swhv.oma.be/availability/images/availability/available/".$image_id.".json";
		echo 'createAvail("IID'.$image_id.'", "'.$url.'");';
	    }
	    $row = mysqli_fetch_row($result);
	}
	mysqli_close($con);
?>
</script>
</body>
