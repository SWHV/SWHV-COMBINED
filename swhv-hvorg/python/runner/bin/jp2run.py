#!/swhv/python/bin/python
import subprocess 
import argparse
import datetime
import os
import sys
import re
import time
import sidcutil.fileutil as fileutil
from multiprocessing.pool import Pool
from runner.runconfig import process_tree_config

def run_process_tree(root_element, command, output_dir, temp_output_dir,  pool):
    res = pool.apply_async(runproc , [root_element, command, output_dir, temp_output_dir])
    res.finished = False
    results.append(res)
    return

def runproc( root_element, command, output_dir, temp_output_dir):
    fileutil.mkdirp(temp_output_dir)
    if temp_output_dir[-1]!='/':
        temp_output_dir = temp_output_dir + '/'
    if output_dir[-1]!='/':
        output_dir = output_dir + '/'
    print( "Running the command: " + command )
    p = subprocess.Popen( command, shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (stdout, stderr) = p.communicate()
    sys.stdout.flush()
    sys.stderr.flush()
    temp_output_files = fileutil.get_recursive_file_list(temp_output_dir)
    temp_output_files 
    output_files = []
    for temp_output_file in temp_output_files:
        output_file = temp_output_file.replace(temp_output_dir, output_dir)
        output_files.append(output_file)
    fileutil.move(output_files, output_dir, temp_output_dir)
    print( "The command has finished: " + command)
    print("The stdout of the program " + str(stdout))
    print( "The stderr of the program " + str(stderr))
    return (output_files, root_element)

def get_process_argument(arglist):
    """Parses the process argument from command line. Returns None if it is not present."""
    i = 0
    while i < len(sys.argv) and ( sys.argv[i] not in arglist):
        i = i+1
    if i < len(sys.argv) and (sys.argv[i] in arglist ) and i+1 < len(sys.argv):
        return sys.argv[i+1]
    return None

def parse_arguments( root_element ):
    if 'command_line_parameters' in root_element:
        parser = argparse.ArgumentParser()
        parser.add_argument('-p', '--process_name')
        for param in root_element['command_line_parameters']:
            parser.add_argument( param[0], param[1], required=True )
        args = vars((parser.parse_known_args())[0])
        for param in root_element['command_line_parameters']:
            param_without_minus = param[1][2:]
            root_element['command'] = root_element['command'].replace('%%'+ param_without_minus + '%%', args[param_without_minus])
    return root_element['command']

def create_command(root_element, input_files, output_file, output_dir, temp_output_file, temp_output_dir):
    command = parse_arguments(root_element)
    if input_files is not None:
        command = command.replace('%%input_file%%', ','.join(input_files))
    if temp_output_file is not None:
        command = command.replace('%%output_file_path%%', temp_output_file)
    if temp_output_dir is not None:
        command = command.replace('%%output_file_dir%%', temp_output_dir)
    if output_dir is not None:
        command = command.replace('%%final_dir%%', output_dir)
    return command

def create_command_parameters(root_element, input_file=None):
    output_dir = root_element['output_dir']
    if output_dir[-1]!= "/":
        output_dir = output_dir + "/"
    input_dir = None
    if root_element['parent'] is not None:
        input_dir = process_tree_config[root_element['parent']]['output_dir']
        output_dir = os.path.dirname(input_file).replace(input_dir, output_dir)
        if input_dir[-1]!= "/":
            input_dir = input_dir + "/"
    now = datetime.datetime.utcnow()
    uid = now.strftime("%Y%m%d%H%M%s") + str(now.microsecond)
    if input_file == None:
        temp_output_dir = os.path.join(root_element['temp_output_dir'] + "/", uid)
    else:
        temp_output_dir = os.path.join(os.path.dirname(input_file).replace(input_dir, root_element['temp_output_dir'])+"/", uid)
    if temp_output_dir[-1]!= "/":
        temp_output_dir = temp_output_dir + "/"
    return (input_dir, output_dir, temp_output_dir)

def run_tree(PROCESSES):
    print('Creating pool with %d processes\n' % PROCESSES)
    pool = Pool(PROCESSES)
    finished = False
    root_element = process_tree_config[get_process_argument(['-p','--process_name'])]
    (input_dir, output_dir, temp_output_dir) = create_command_parameters(root_element)
    input_files = None
    output_file = None
    temp_output_file = None
    begin_date = get_process_argument(['-b','--begin_date'])
    end_date = get_process_argument(['-e','--end_date'])
    command = create_command(root_element, input_files, output_file, output_dir, temp_output_file, temp_output_dir)
    run_process_tree(root_element, command, output_dir, temp_output_dir, pool)
    results_queue = []
    while( not finished ):
        finished = True
        toRemove = []
        for i, res in enumerate(results):
            ready = res.ready() and res.successful()
            finished = finished and ready
            if ready:
                toRemove.append(i)
        toRemove.sort();
        lenrem = len(toRemove)
        for i in range(lenrem):
            results_queue.append(results.pop(lenrem -1 - i))
        while len(results_queue)>0:
            res = results_queue.pop(0)
            (output_files, root_element) = res.get()
            reprocess = False
            if 'reprocess' in root_element:
                reprocess = root_element['reprocess']
            if begin_date is not None and end_date is not None and root_element['first'] and reprocess:
                existing_list = fileutil.get_recursive_file_list(root_element['output_dir'])
                existing_list.extend(output_files)
                existing_datefiles = fileutil.parse_date_files(root_element['filename_date_pattern'], existing_list, output_dir + root_element['output_dir_date_format_add'], "")
                existing_datefiles = fileutil.truncate_date_files(existing_datefiles, fileutil.parse_date(begin_date), fileutil.parse_date(end_date))
                existing_list = []
                for existing_file in existing_datefiles:
                    existing_list.append(existing_file.path)
                output_files = existing_list
            for child_txt in root_element['children']:
                child = process_tree_config[child_txt]
                if 'run_once' in child and child['run_once']:
                        finished = False
                        input_files = output_files
                        if len(input_files)>0:
                            (input_dir, output_dir, temp_output_dir) = create_command_parameters(child, input_file = input_files[0])
                            inputf = input_files[0]
                            output_file = os.path.join(output_dir +"/", os.path.basename(inputf))
                            temp_output_file = os.path.join(temp_output_dir + "/", os.path.basename(inputf))
                            command = create_command(child, input_files, output_file, output_dir, temp_output_file, temp_output_dir)
                            run_process_tree(child, command, output_dir, temp_output_dir, pool)
                else:
                    for output_file in output_files:
                        torun = True
                        if 'filename_filter_pattern' in child:
                            pattern = re.compile(child['filename_filter_pattern'])
                            if re.match(pattern, os.path.basename(output_file))==None:
                                torun = False
                        finished = False
                        if torun:
                            input_files = [output_file]
                            (input_dir, output_dir, temp_output_dir) = create_command_parameters(child, input_file = input_files[0])
                            inputf = input_files[0]
                            output_file = os.path.join(output_dir +"/", os.path.basename(inputf))
                            temp_output_file = os.path.join(temp_output_dir + "/", os.path.basename(inputf))
                            command = create_command(child, input_files, output_file, output_dir, temp_output_file, temp_output_dir)
                            run_process_tree(child, command, output_dir, temp_output_dir, pool)

                        input_files = None
                        output_file = None
                        output_dir = None
                        temp_output_file = None
                        temp_output_dir = None
                child = None
        time.sleep(0.01)

if __name__=='__main__':
    PROCESSES = 4
    results = []
    run_tree(PROCESSES)
