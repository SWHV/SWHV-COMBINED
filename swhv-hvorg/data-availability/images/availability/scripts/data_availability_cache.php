<?php
    date_default_timezone_set('UTC');
    $con = mysqli_connect("swhv.oma.be","helioviewer","helioviewer","helioviewer");
    if (mysqli_connect_errno())
    {
        echo "Failed to connect to database: " . mysqli_connect_error();
    }
    function readJsonFile($filename){
        if( !file_exists($filename) ){
            return array();
        }
        $filecontents = file_get_contents($filename);
        if($filecontents!=""){
            $json = json_decode($filecontents, true);
            return $json["coverage"];
        }
        else{
            return array();
        }
    }
    function getFilenameAndGapFromDb($con, $imagesrc){
        $query="SELECT regularCadenceSeconds, id FROM helioviewer.datasources where name='".$imagesrc."'";
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_row($result);
        if($row){
                $regularCadence = $row[0];
                $gap = floatval($regularCadence)*5;
                $image_id = $row[1];
                $filename = "/available/".$image_id.".json";
                return array("filename"=>$filename, "gap"=>$gap, "id"=>$image_id);
        }
        return array("filename"=>"", gap=>null);
    }    
    parse_str(implode('&', array_slice($argv, 1)), $_GET);
    $imagesrc= strip_tags($_GET["imagesrc"]);
    $filename_gap = getFilenameAndGapFromDb($con, $imagesrc);
    $image_id = $filename_gap['id'];
    if($filename_gap["filename"]==""){
            //header('Content-type: application/json');
            echo json_encode(array( "error"=>"The requested datasource does not exist.")); 
    }
    else{
        $coverage = readJsonFile($filename_gap["filename"]);
        $i = max(0,count($coverage)-1);
        $found = false;
        $now = time();
        $query = "";
        while($i>0 &&!$found){
            if($now - $coverage[$i][0] > 60*60*24*3){
                $found = true;
                $start_date = strftime("%Y-%m-%d %H:%M:%S", $coverage[$i][0]);
                $query="SELECT date FROM helioviewer.images where sourceId = '".$image_id."' AND date >= '".$start_date."' ORDER BY date";
                unset($coverage[$i]);
                $i = $i+1;
            }
            else{
                unset($coverage[$i]);
            }
            $i--;
        }
        if($query==""){
            $query="SELECT date FROM helioviewer.images where sourceId = '".$image_id."' ORDER BY date";
        }
        $result = mysqli_query($con, $query);
        $coverage_subarray = array();
        $firsttime = true;
        while($row = mysqli_fetch_row($result))
        {
            if($firsttime){
                $prevdt = strtotime($row[0]);
                $firsttime = false;
                $coverage_subarray[0] = $prevdt;
            }
            $dt = strtotime($row[0]);
            if($dt - $prevdt> $filename_gap['gap']){
                    $coverage_subarray[1] = $prevdt;
                    $coverage[$i] =  $coverage_subarray;
                    $i = $i+1;
                    $coverage_subarray[0] = $dt;
            }
            $prevdt = $dt;
        }
        if(!$firsttime){
            $coverage_subarray[1] = $dt;
            $coverage[$i] =  $coverage_subarray;
            $i = $i+1;
        }
        //header('Content-type: application/json');
        $ja = json_encode(array( "coverage"=>$coverage)); 
        file_put_contents(dirname(__FILE__).$filename_gap['filename'],$ja);
      }
      mysqli_close($con);
