SHELL=/bin/bash

MAIN_DIR=$(abspath $(dir $(CURDIR)/))
READVAR=cd $(MAIN_DIR)/iniparser &&  $(MAIN_DIR)/iniparser/parse
READMYSQLVAR=$(READVAR) $(MAIN_DIR)/combined.env mysql
MYSQL_HOST=$(shell $(READMYSQLVAR) MYSQL_HOST)
MYSQL_PORT=$(shell $(READMYSQLVAR) MYSQL_PORT)

READ_HVORG_VAR=$(READVAR) $(MAIN_DIR)/combined.env hvorg
NGINX_PORT=$(shell $(READ_HVORG_VAR) NGINX_PORT)
ESA_JPIP_HOST=$(shell $(READ_HVORG_VAR) ESA_JPIP_HOST)
ESA_JPIP_PORT=$(shell $(READ_HVORG_VAR) ESA_JPIP_PORT)

SWHV_PIPELINES_HOST=swhv-pipelines

CONFIG_DIR=$(MAIN_DIR)/config
DOCKERTARGET=swhv/TARGET:latest
SWHVNETWORK=swhvnetwork

DOCKERRUN=docker run -d --rm -it --network $(SWHVNETWORK) --name=TARGET --hostname=TARGET
MOUNTSWHVCONFIG=--mount type=bind,src=$(MAIN_DIR)/config,dst=/swhv-config
MOUNTWAITFORIT=--mount type=bind,src=$(MAIN_DIR)/external/wait-for-it,dst=/wait-for-it

UID:=$(shell id -u)
#GID:=$(shell id -g)
GID:=1000
UNAME:=swhv

all: build

install: restart

BUILDCONTAINERS:=swhv-hvorg $(ESA_JPIP_HOST) $(SWHV_PIPELINES_HOST)
ALLCONTAINERS:=$(MYSQL_HOST) swhv-hvorg $(ESA_JPIP_HOST) $(SWHV_PIPELINES_HOST)

build: initsubmodules $(addprefix build_,$(BUILDCONTAINERS))

initsubmodules: 
	git submodule init && git submodule update

clean: stop $(addprefix clean_,$(BUILDCONTAINERS))

stop: $(addprefix stop_,$(ALLCONTAINERS)) stopnetwork

start: startnetwork $(addprefix start_,$(ALLCONTAINERS))

restart: stop start

build_%:
	echo "Building $@"
	docker build -q --build-arg UID=$(UID) --build-arg GID=$(GID) --build-arg UNAME=$(UNAME) --rm -t swhv/$*:latest --file $*/Dockerfile $(MAIN_DIR)

clean_%:
	docker rmi swhv/$*:latest

stop_%:
	STOPCHK=$$(docker ps -q -f status=running -f name=$*); \
	if [ -n "$${STOPCHK}" ]; then docker stop $${STOPCHK}; fi;
	RMCHK=$$(docker ps -aq -f status=exited -f name=$*); \
	if [ -n "$${RMCHK}" ]; then docker rm $${RMCHK}; fi

startnetwork:
	docker network create $(SWHVNETWORK)

stopnetwork:
	STOPCHK=$$(docker network ls -f name=$(SWHVNETWORK) --format "{{.ID}}"); \
	if [ -n "$${STOPCHK}" ]; then docker network rm $(SWHVNETWORK); fi;

start_swhv-hvorg:
	$(subst TARGET,swhv-hvorg,$(DOCKERRUN)) \
		-p 0.0.0.0:8001:80/tcp \
		--env-file $(CURDIR)/combined.env \
		-v /data:/data \
    -v $(CURDIR)/external/wait-for-it:/wait-for-it \
		$(subst TARGET,swhv-hvorg,$(DOCKERTARGET)) \
		bash -c "/wait-for-it/wait-for-it.sh swhv-mysql:3306 --timeout=20 --strict -- /scripts/init && /usr/bin/supervisord"

start_swhv-mysql:
	$(subst TARGET,$(MYSQL_HOST),$(DOCKERRUN)) \
    -p 127.0.0.1:$(MYSQL_PORT):3306/tcp \
    --env-file $(CURDIR)/combined.env \
		-v /data/swhv/mysqljpipdata:/var/lib/mysql \
    mysql:5.7

start_swhv-esajpip:
	$(subst TARGET,$(ESA_JPIP_HOST),$(DOCKERRUN)) \
		-p 0.0.0.0:$(ESA_JPIP_PORT):$(ESA_JPIP_PORT)/tcp \
		--env-file combined.env \
		-v /data:/data \
		$(subst TARGET,$(ESA_JPIP_HOST),$(DOCKERTARGET))

start_swhv-pipelines:
	$(subst TARGET,$(SWHV_PIPELINES_HOST),$(DOCKERRUN)) \
		-v /data:/data \
		--env-file combined.env \
		$(subst TARGET,$(SWHV_PIPELINES_HOST),$(DOCKERTARGET))

initialize_database:
	docker exec -it swhv-pipelines /scripts/init
reingest:
	docker exec -it swhv-pipelines /scripts/reingest
