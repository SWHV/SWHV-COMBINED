<!DOCTYPE html>
<html>
<head>
</head>
<body>
<?php
date_default_timezone_set('UTC');

$con = mysqli_connect("localhost","helioviewer","helioviewer","helioviewer");
if (mysqli_connect_errno())
{
    echo "Failed to connect to database: " . mysqli_connect_error();
}

$query="SELECT id, name, description FROM helioviewer.datasources";
$result = mysqli_query($con, $query);
$row = mysqli_fetch_row($result);
while($row){
    $image_id = $row[0];
    $filename = "./available/".$image_id.".json";
    $cont = file_get_contents($filename,$ja);
    if($cont){
	echo '<h2>';
	echo $row[2];
	echo '</h2>';
	$cont_parsed = json_decode($cont, true);
	$coverage = $cont_parsed["coverage"];
	for($i=0; $i<count($coverage);$i++){
	    echo "[";
	    echo strftime("%Y-%m-%d %H:%M:%S", $coverage[$i][0]);
	    echo ", ";
	    echo strftime("%Y-%m-%d %H:%M:%S", $coverage[$i][1]);
	    echo "]";
	    echo "<br>";
	}
    }
    $row = mysqli_fetch_row($result);
}
mysqli_close($con);
?>
</body>
</html>
