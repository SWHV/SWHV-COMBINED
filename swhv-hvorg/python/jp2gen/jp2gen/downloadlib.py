import argparse
import urllib2
import datetime
import re
import os
import sys
import sidcutil.fileutil as fileutil
import string
import logging



class WrapCurl():
    """Parallel download, set limitjobs for the max number of concurrent downloads. It will create
       the directories in the outputfile field of the DateFile list. The download list should be a list
       of DateFile objects. If checkexist is True it will check the existence of the output files and not redownload them.
       If create_directory is true, the directories where the output files are put will be created.
    """
    def __init__(self, downloadlist, limitjobs = 8, checkexist = True, create_directory = True):
        """Constructor. Sets the download list"""
        self.downloadlist = downloadlist
        self.checkexist = checkexist
        #Limit of max concurrent downloads
        self.limitjobs = limitjobs
        self.childprocesses = {}
        #Current list of queue
        self.jobs = []
        self.curl = ["curl", "-s", "-o"]
        if create_directory:
            self.create_directories()
        if self.checkexist:
            helplist = []
            for download in self.downloadlist:
                if (not os.path.exists(download.outputfile)) and (not os.path.exists(string.replace(download.outputfile,"localjp2/", "jp2/"))):
                    helplist.append(download)
            self.downloadlist = helplist

    def create_directories(self):
        """Create the directories."""
        for date_file in self.downloadlist:
            fileutil.mkdirp( os.path.dirname(date_file.outputfile) )

    def spawn(self, cmd, *args):
        """Spawn a new download process."""
        argv = [cmd] + list(args)
        pid = None
        try:
            pid = os.spawnlp(os.P_NOWAIT, cmd, *argv)
            self.childprocesses[pid] = {'pid': pid, 'cmd': argv}
        except Exception, inst:
            logging.error( "The download of cmd %s with pid %d failed."% ( str(cmd), pid )  )
        #logging.info( "spawned pid %d of nproc=%d njobs=%d for '%s'" % (pid, len(self.childprocesses), len(self.jobs), " ".join(argv)) )
        return pid

    def rundownload(self):
        """Run and watch the parallel downloads."""
        for download in self.downloadlist:
            cmd = self.curl + [download.outputfile, download.url]
            self.jobs.append(cmd)
        logging.info( "Currently %d curl jobs are queued for downloading" % len(self.jobs) )

        # Spawn at most self.limitjobs in parallel.
        while len(self.jobs) > 0 and len(self.childprocesses) < self.limitjobs:
            cmd = self.jobs[0]
            if self.spawn(*cmd):
                del self.jobs[0]
        #logging.info( "Added %d jobs for download" % len(self.childprocesses) )

        # Watch for dying self.childprocesses and keep spawning new jobs while
        # we have them, in an effort to keep <= self.limitjobs self.childprocesses active.
        while len(self.jobs) > 0 or len(self.childprocesses):
            (pid, status) = os.wait()
            #logging.info( "Download job with pid %d succesfully completed. status=%d, nproc=%d, njobs=%d, cmd=%s" % \
            #    (pid, status, len(self.childprocesses) - 1, len(self.jobs), \
            #     " ".join(self.childprocesses[pid]['cmd']))
            #)
            del self.childprocesses[pid]
            if len(self.childprocesses) < self.limitjobs and len(self.jobs):
                cmd = self.jobs[0]
                if self.spawn(*cmd):
                    del self.jobs[0]

class DownloadFileLists():
    """Fetches the needed download from an url by first getting its listing. The cadence is the time interval between the files in seconds.
       The date is the date for which the files need to be fetched. The download_config expresses the configuration.
       Lastdate is the first date after which a download is allowed.
       Add_to_year is used when the year is incomplete in the filename (e.g. 06 instead of 2006).
    """
    def __init__( self, download_config, cadence,  begin_date, end_date, output_dir, final_dir = None ):
        self.output_dir = output_dir
        self.final_dir = final_dir
        self.begin_date = begin_date
        self.end_date = end_date
        self.download_config = download_config
        #self.output_dir = output_dir
        self.dtcadence = datetime.timedelta( seconds = cadence )
        self.cadence = cadence
        #The urls for the file lists
        self.urls = []
        self.create_urls()
        #The downloaded urls
        self.listings = []
        self.download_file_lists()
        #The list of files to download
        self.file_list = []
        self.file_list = self.parse_filenames( self.listings )
        #The list of files with date
        self.date_file_list = []
        self.date_file_list = fileutil.parse_date_files( self.download_config['filename_date_pattern'], \
                                                         self.file_list, self.output_dir + self.download_config['output_dir_date_format_add'],\
                                                         self.download_config['base_url_format']
                                                       )
        self.date_file_list = fileutil.sort_date_files( self.date_file_list )
        #logging.debug( "The sorted list of available in the file listing " + str(self.date_file_list) )
        self.download_list = []
        self.get_correct_cadence()

    def get_url_list(self):
        return self.date_file_list

    def create_urls(self):
        date = self.begin_date
        while date < self.end_date:
            url = date.strftime( self.download_config['base_url_format'] )
            self.urls.append(url)
            date = date + datetime.timedelta(1)

    def download_file_lists(self):
        """Downloads the file listings into self.listing."""
        self.listings = []
        for url in self.urls:
            listing = ""
            try:
                f = urllib2.urlopen( url, timeout = 20 )
            except:
                logging.warning( "The url " + url + "does not exist.\n" )
            else:
                listing = f.read()
                self.listings.append( listing )


    def parse_filenames(self, inputlist):
        """parses the filenames from the listing according to the given pattern."""
        file_list = []
        pattern = re.compile( self.download_config['filename_pattern'] )
        for listing in inputlist:
            file_list = file_list + pattern.findall( listing )
        file_list = list(set(file_list))
        file_list = sorted(file_list)
        return file_list


    def get_correct_cadence(self):
        """get the correct date cadence for the given files. And change the input files accordingly"""
        if self.final_dir is not None:
            self.existing_list = fileutil.get_recursive_file_list(self.final_dir)
        else:
            self.existing_list = []
        self.existing_list = fileutil.parse_date_files(self.download_config['filename_date_pattern'],\
                                                       self.existing_list, self.output_dir + self.download_config['output_dir_date_format_add'],\
                                                       self.download_config['base_url_format']
                                                       )
        self.existing_list = fileutil.sort_date_files(self.existing_list)
        if(self.cadence>0): 
            i = 0
            max_index = len(self.existing_list)
            helplist  = self.existing_list
            imax = len(helplist)
            for el in self.date_file_list:
                while(i<imax and el.date>=helplist[i].date):
                    i = i+1
                if imax==0\
                   or (i==0 and helplist[0].date-el.date>self.dtcadence)\
                   or (i==imax and el.date - helplist[imax-1].date>self.dtcadence)\
                   or (i!=imax and i!=0 and helplist[i].date-el.date>self.dtcadence and el.date-helplist[i-1].date>self.dtcadence):
                    if len(self.download_list)==0 or el.date-self.download_list[-1].date>self.dtcadence:
                        self.download_list.append(el)
        else:
            self.download_list = self.date_file_list
