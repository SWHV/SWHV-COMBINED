#!/swhv/python/bin/python
from jp2gen.downloadlib import DownloadFileLists, WrapCurl
import sidcutil.fileutil as fileutil
import argparse
import datetime
import re
from jp2gen.downloadconfig import config


def convert_to_datetime( date_string ):
    """Converts Y-m-d string into datetime object. Performs no check on validity, just throws an error."""
    try:
        date = fileutil.parse_date(date_string)
    except:
        raise('This given date could not be parsed.')
    return date

def parse_arguments():
    """Parses the command line arguments and performs some basic checks. Returns a parsed list of arguments."""

    description = """This program is downloading images from various channels. Please read the source to see all options."""
    parser = argparse.ArgumentParser(description = description)
    parser.add_argument( '-t', '--type', help = 'The type of image. Use one of gong_halpha,', required = True )
    parser.add_argument( '-b', '--begin_date', help = 'The end date in format Y-m-d', required = True )
    parser.add_argument( '-e', '--end_date', help = 'The begin date in format Y-m-d', required = True )
    parser.add_argument( '-o', '--output_dir', help = 'The path to the output dir', required = True )
    parser.add_argument( '-f', '--final_dir', help = 'The directory where the list of existing files is checked', required = False )
    parser.add_argument( '-c', '--cadence', help = 'The wanted cadence in seconds', required = True )
    parser.add_argument( '-s', '--station', help = 'The wanted station', required = False )
    parser.add_argument( '-m', '--stations', help = 'The wanted stations', required = False )
    args = vars(parser.parse_args())
    return args

if __name__ == '__main__':
    args = parse_arguments()
    if args['type'] not in config:
        raise "The given type does not exist"
    try:
        cadence = int(args['cadence'])
        if cadence==0:
            cadence = -1
    except:
       raise("please specify an integer for the cadence (seconds)")

    download_config = config[ args['type'] ]
    file_list = []
    begin_date = convert_to_datetime(args['begin_date'])
    end_date = convert_to_datetime(args['end_date'])
    final_dir = None
    if 'final_dir' in args:
        final_dir = args['final_dir']
    output_dir = args['output_dir']

    if 'stations' in args and args['stations'] is not None:
        base_url = download_config['base_url_format']
        for station in args['stations'].split(','):
            download_config['base_url_format'] = base_url.replace('%%station%%', station)
            overview = DownloadFileLists( download_config, cadence, begin_date, end_date, output_dir, final_dir=final_dir )
            file_list.extend( overview.download_list )
    elif 'station' in args and args['station'] is not None:
        download_config['base_url_format'] = download_config['base_url_format'].replace('%%station%%', args['station'])
        overview = DownloadFileLists( download_config, cadence, begin_date, end_date, output_dir, final_dir=final_dir )
        file_list.extend( overview.download_list )
    else:
        overview = DownloadFileLists( download_config, cadence, begin_date, end_date, output_dir, final_dir = final_dir )
        file_list.extend( overview.download_list )
    downloader = WrapCurl( file_list, limitjobs = download_config['limitjobs'],\
                           checkexist = download_config['checkexist'],\
                           create_directory = download_config['create_directory']
                           )
    downloader.rundownload()
