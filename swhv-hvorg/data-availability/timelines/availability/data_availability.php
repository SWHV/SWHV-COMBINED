<!DOCTYPE html>
<html>
<head>
</head>
<body>
<?php
include("lookup.php");
date_default_timezone_set('UTC');
$datasets_array_internal = get_lookup_array_datasets();

for ($i = 0; $i < count($datasets_array_internal); ++$i) {
    if(array_key_exists("dbtable", $datasets_array_internal[$i])){
	    $image_id = $datasets_array_internal[$i]["dbtable"];
	    $filename = "./available/".$image_id.".json";
	    echo '<h2>';
	    echo $datasets_array_internal[$i]["name"];
	    echo '</h2>';
	    if(file_exists($filename)){
		$cont = file_get_contents($filename);
		$cont_parsed = json_decode($cont, true);
		$coverage = $cont_parsed["coverage"];
		for($j=0; $j<count($coverage);$j++){
		    echo "[";
		    echo strftime("%Y-%m-%d %H:%M:%S", $coverage[$j][0]);
		    echo ", ";
		    echo strftime("%Y-%m-%d %H:%M:%S", $coverage[$j][1]);
		    echo "]";
		    echo "<br>";
		}
	    }
    }
}
?>
</body>
</html>
