import numpy as np


def bin( orig_ax, bins, data_array ):
    """orig_ax contains the tickpoints of the original axis; bins contains the tickpoints of a bin.
        Given two subsequent points a,b of the bin, this function averages all tickpoints int the orig_ax that lie between a and b
        If a or b is exactly a tickpoint, it's weight is only 0.5 in the entire average.
        In case a multidimensional data_array is given, the averaging is done in the first dimension.
        Example:
        v = np.random.rand(11,11)
        #v = np.ones((11,11))
        #v = np.identity(11)
        #Binning in the rows
        v = bin(  np.arange(11), np.array([0,0.1,4,9]), v)
        #Transpose to bin in the columns
        v = v.transpose()
        v = bin( np.arange(11), np.array([0,4,9]), v)
        #Transpose again to go back to original
        v = v.transpose()
        """
    bins_size = bins.size
    index = 0
    orig_ax_size = orig_ax.shape[0]
    count = 0
    shape = []
    for i in data_array.shape:
        if count==0:
            count = 1
            shape.append(bins_size)
        else:
            shape.append(i)
    
    binned_array = np.zeros(shape)
    total_weight = 0
    for i in range(bins_size):
        while( index < orig_ax_size and orig_ax[index] < bins[i] ):
            if i>0 and orig_ax[index] == bins[i-1]:
                total_weight = total_weight + 0.5
                binned_array[i] += data_array[index]/2.0
            else:
                binned_array[i] += data_array[index]
                total_weight = total_weight + 1
            index = index + 1
        if index < orig_ax_size and orig_ax[index] == bins[i]:
            total_weight = total_weight + 0.5
            binned_array[i] += data_array[index]/2.0
        if total_weight!=0:
            binned_array[i] /= total_weight
        total_weight = 0
    return binned_array[1:]

def median_bin( orig_ax, bins, data_array ):
    """orig_ax contains the tickpoints of the original axis; bins contains the tickpoints of a bin.
        Given two subsequent points a,b of the bin, this function medianizes all tickpoints int the orig_ax that lie between a and b
        If a or b is exactly a tickpoint, it is counted in the entire median.
        In case a multidimensional data_array is given, the medianization is done in the first dimension.
        Example:
        v = np.random.rand(11,11)
        #v = np.ones((11,11))
        #v = np.identity(11)
        #Binning in the rows
        v = median_bin(  np.arange(11), np.array([0,0.1,4,9]), v)
        print v
        #Transpose to bin in the columns
        v = v.transpose()
        print v
        v = median_bin( np.arange(11), np.array([0,4,9]), v)
        print v
        #Transpose again to go back to original
        v = v.transpose()
        """
    bins_size = bins.size
    index = 0
    orig_ax_size = orig_ax.shape[0]
    count = 0
    shape = []
    for i in data_array.shape:
        if count==0:
            count = 1
            shape.append(bins_size)
        else:
            shape.append(i)
    binned_array = np.zeros(shape)
    for i in range(bins_size):
        help_data_index_arr = []
        while( index < orig_ax_size and orig_ax[index] < bins[i] ):
            help_data_index_arr.append(index)
            index = index + 1
        if index < orig_ax_size and orig_ax[index] == bins[i]:
            help_data_index_arr.append(index)
        if len(help_data_index_arr)>0:
            binned_array[i] = np.median( data_array[help_data_index_arr], axis=0)
    return binned_array[1:]

