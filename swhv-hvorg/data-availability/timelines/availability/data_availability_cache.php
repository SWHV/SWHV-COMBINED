<?php
include("lookup.php");
ini_set('memory_limit','512M');
    date_default_timezone_set('UTC');

    date_default_timezone_set('UTC');
    $con = mysqli_connect("localhost","user3","pw3","ODI");
    if (mysqli_connect_errno())
    {
        echo "Failed to connect to database: " . mysqli_connect_error();
    }
    function readJsonFile($filename){
        if( !file_exists($filename) ){
            return array();
        }
        $filecontents = file_get_contents($filename);
        if($filecontents!=""){
            $json = json_decode($filecontents, true);
            return $json["coverage"];
        }
        else{
            return array();
        }
    }
    function getFilenameAndGapFromDb($con, $imagesrc){    
        $datasets_array_internal = get_lookup_array_datasets();
	$found = false;
	$max = count($datasets_array_internal);
	$i = 0;
	while(!$found && $i<$max) {
	    if(array_key_exists("dbtable", $datasets_array_internal[$i])){
		if($datasets_array_internal[$i]["dbtable"]==$imagesrc){
		    $found = true;
		}
		else{
		    $i = $i+1;
		}
	    }
	    else{
		$i = $i + 1;
	    }
	}
	if(!$found){
	    mysqli_close($con);
	    die("Dataset does not exist: ".$imagesrc."\n");
	}
        return array("dbtable"=>$datasets_array_internal[$i]["dbtable"], "gap"=>60*10);
    }    
    parse_str(implode('&', array_slice($argv, 1)), $_GET);
    $imagesrc= strip_tags($_GET["imagesrc"]);
    $filename_gap = getFilenameAndGapFromDb($con, $imagesrc);
    if($filename_gap["dbtable"]==""){
            echo json_encode(array( "error"=>"The requested datasource does not exist.")); 
    }
    else{
	//Read existing coverage intervals
        $coverage = readJsonFile(dirname(__FILE__)."/available/".$filename_gap['dbtable'].".json");
        $i = max(0, count($coverage)-1);
        $found = false;
        $now = time();
	$query = "";
	//Remove last intervals for 3 days
        while($i>0 &&!$found){
            if($now - $coverage[$i][0] > 60*60*24*3){
                $found = true;
		$start_date = strftime("%Y-%m-%d %H:%M:%S", $coverage[$i][0]);
		$dbtable = $filename_gap['dbtable'];
		$query="SELECT epoch FROM ODI.".$dbtable." where epoch >= '".$start_date."' ORDER BY epoch;";
                unset($coverage[$i]);
                $i = $i+1;
            }
            else{
                unset($coverage[$i]);
            }
            $i--;
	}
	//$i contains number of intervals
	//Create query if not existing
        if($query==""){
	    $query="SELECT epoch FROM ODI.".$filename_gap['dbtable']." ORDER BY epoch;";
	}
        $result = mysqli_query($con, $query);
        $coverage_subarray = array();
        $firsttime = true;
        while($result && $row = mysqli_fetch_row($result))
        {
            if($firsttime){
                $prevdt = strtotime($row[0]);
                $firsttime = false;
                $coverage_subarray[0] = $prevdt;
            }
            $dt = strtotime($row[0]);
            if($dt - $prevdt> $filename_gap['gap']){
                    $coverage_subarray[1] = $prevdt;
                    $coverage[$i] =  $coverage_subarray;
                    $i = $i+1;
                    $coverage_subarray[0] = $dt;
            }
            $prevdt = $dt;
        }
        if(!$firsttime){
            $coverage_subarray[1] = $dt;
            $coverage[$i] =  $coverage_subarray;
            $i = $i+1;
        }
        $ja = json_encode(array( "coverage"=>$coverage)); 
        file_put_contents(dirname(__FILE__)."/available/".$filename_gap['dbtable'].".json",$ja);
      }
      mysqli_close($con);
