#!/usr/bin/env python
from setuptools import setup, find_packages
scripts = ['bin/pipeline_callisto', 'bin/pipeline_gong_farside', 'bin/pipeline_gong_magnetogram', \
        'bin/pipeline_nrh', 'bin/pipeline_solis', 'bin/pipeline_callisto_10s',  'bin/pipeline_gong_halpha',\
        'bin/pipeline_kanzelhoehe', 'bin/pipeline_rob_uset', 'bin/jp2run.py']

kwargs = {'name': 'runner',
          'description': 'Convert fits files to accomodate SWHV processing',
          'author': 'ROB',
          'packages': find_packages(),
          'scripts': scripts,
         }

instllrqrs = []
kwargs['install_requires'] = instllrqrs

clssfrs = ["Programming Language :: Python",
           "Programming Language :: Python :: 2.7",
           "Development Status :: 5 - Production/Stable",
           "Intended Audience :: Science/Research",
           "Intended Audience :: Information Technology",
           "Topic :: Software Development :: Libraries :: Python Modules"]
kwargs['classifiers'] = clssfrs
kwargs['version'] = '0.1'

setup(**kwargs)
