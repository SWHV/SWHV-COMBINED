#!/swhv/python/bin/python
import numpy as np
from astropy.io import fits
import ephem
import argparse
import logging

def process_nrh(args):
    filename = arguments['input_file']
    logging.debug("Processing" + filename)
    #read original nrh fits file
    hdulist=fits.open(filename)
    head0=hdulist[0].header
    head1=hdulist[1].header
    data=hdulist[1].data
    hdulist.close()
    #select intensity
    stokesi=data['STOKESI']
    nb_img=head1['NAXIS2']
    #time array
    time_arr=data['TIME']/1000.
    #ephemeris calculation for b0. taken from solarsoft pb0r.pro
    jd=ephem.julian_date(head1['DATE-OBS']+' '+head1['TIM_STR'])
    de=jd-2415020.
    sun=ephem.Sun()
    sun.compute(head1['DATE-OBS']+' '+head1['TIM_STR'])
    shlon=sun.hlong.real*180./ephem.pi
    lamda=shlon-(20.5/3600.)
    node=73.666666+(50.25/3600.)*((de/365.25)+50.0)
    arg=lamda-node
    #don't know why, but I have to multiply by -1
    b0=(-1)*np.arcsin(0.12620*np.sin(arg*ephem.pi/180.))*180./ephem.pi
    for i in range(nb_img):
        hdu=fits.PrimaryHDU(np.clip(stokesi[i,:,:],0,stokesi[i,:,:].max())*(1000000/stokesi[i,:,:].max()))
        hdulist=fits.HDUList([hdu])
        hduhdr= hdulist[0].header
        hduhdr['TELESCOP']="NRH"
        hduhdr['INSTRUME']="NRH2"
        hduhdr['DETECTOR']="NRH2"
        hduhdr['WAVELNTH']=np.str(head1['FREQ'])+'MHz'#np.str(head0['WAVELNT'])
        hh=np.floor(time_arr[i]/3600)
        mm=np.floor(((time_arr[i]/3600.)-hh)*60)
        ss=((time_arr[i]/3600.)-hh)*60-mm
        hduhdr['DATE-OBS']=head0['DATE-OBS']+"T"+"%02d"%hh+':'+"%02d"%mm+':'+\
                           "%06.3f"%ss+'Z'
        hduhdr['CRPIX1']=head1['CRPIX1']
        hduhdr['CRPIX2']=head1['CRPIX2']
        hduhdr['CDELT1']=sun.radius.real*180./ephem.pi*3600./head1['SOLAR_R']
        hduhdr['CDELT2']=sun.radius.real*180./ephem.pi*3600./head1['SOLAR_R']
        hduhdr['DSUN_OBS']=sun.earth_distance*ephem.meters_per_au
        hduhdr['HGLT_OBS']=b0
        hduhdr['HGLN_OBS']=0.
        output_filename = args['output_dir']+"%4d"%(head1['FREQ']*10)+'D'+head1['DATE-OBS'].replace('-','')+"%02d"%hh+"%02d"%mm+"%02d"%ss+'.fits'
        hdulist.writeto(output_filename, overwrite = True) 
        hdulist.close()
    return

def parse_arguments():
    parser = argparse.ArgumentParser( description='This programs adds some extra SWHV keywords to the given fitsfile.' )
    parser.add_argument( '-i','--input_file', help = 'The name of the inputfile', required = True )
    parser.add_argument( '-o','--output_dir', help = 'The name of the output directory', required = False )
    parser.add_argument( '-v','--verbose', help = 'Verbose info logging', required = False )
    args = vars(parser.parse_args())
    if(not('output_dir' in args) or args['output_dir']==None):
        args['output_dir'] = "/data/temp/fits2fits/nrh/"
    return args

arguments = parse_arguments()
dum=process_nrh( arguments)


